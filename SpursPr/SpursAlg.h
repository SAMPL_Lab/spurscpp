#pragma once
#include "../SuiteSparse/CHOLMOD/Include/cholmod.h"

// The main function 
// prepares the inputs, 
// reads the samples and coordinates from file and call to SpursAlg function
// for chomod_dense and cholmod_common types see cholmod pachage from SuiteSparse // library
// input parameters:
// int numIterations - number of algorithm iteration (default: 20)
// int sqrtN - points per row / column of the cartesian grid (default: 256)
// int overGridFactor - over sampling factor (default: 2)
// double rho - regularization parameter (default: 0.001)
void SpursAlg(cholmod_dense* bSamp, double* SamplingGridCoordinates1, double* SamplingGridCoordinates2,
	int overGridFactor, int sqrtN, double rho, int numIterations, cholmod_common& c);