#include "ReturnFilter.h"
#include <math.h>       /* sin */
#include <stdio.h>

#define PI 3.14159265
double Sinc(const double x)
{
	if (x == 0)
		return 1;
	return sin(PI*x) /(PI* x);
}

double* ReturnFilter(int kernelFunctionDegree, int overGridFactor, int sqrtN)
{
	double initialVal = (double)-sqrtN / 2;
	double finalVal = (double)sqrtN / 2.0 - 1.0 /(double) overGridFactor;
	double step = 1.0 / (double)overGridFactor;
	int numSteps = sqrtN * overGridFactor;
	double* R_AQA = new double[numSteps];
	double* ImgFilter = new double[numSteps*numSteps];
	double xx = initialVal - step; //!=0
	
	for (int i = 0; i < numSteps; i++)
	{
		xx = xx + step;
		double temp = xx / sqrtN*overGridFactor;
		double r_AQ = pow(Sinc(temp), (kernelFunctionDegree +1));
		double r_AA = (temp > -0.5 && temp < 0.5) ? 1 : 0;
		R_AQA[i] = r_AQ * r_AA;
	}
	for (int i = 0; i < numSteps; i++)
	{
		for (int j = 0; j < numSteps; j++)
		{
			ImgFilter[i*numSteps + j] = R_AQA[i] * R_AQA[j];
		}
	}
	return ImgFilter;
}
