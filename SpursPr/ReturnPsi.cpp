#include "ReturnPsi.h"
#include "..\SuiteSparse\UMFPACK\Include\umfpack.h"
#include <string>
#include "BsplinePHI.h"


/* The MatrixMarket format specificies a maximum line length of 1024 */
#define MAXLINE 1024
/* Read one line of the file, return TRUE if successful, FALSE if EOF. */
static int getLine(FILE *f, char *buf)
{
	buf[0] = '\0';
	buf[1] = '\0';
	buf[MAXLINE] = '\0';
	return (fgets(buf, MAXLINE, f) != NULL);
}

void ReadAndAllocateMatrix(FILE *f, sparse_zi_struct* ziArr)
{
	char buf[MAXLINE + 1];
	int i = 0;
	getLine(f, buf);
	int rowNum, colNum, nzNum;
	sscanf_s(buf, "%d %d %d\n", &rowNum, &colNum, &nzNum);
	ziArr->nrow = rowNum;
	ziArr->ncol = colNum;
	ziArr->nzmax = nzNum;
	ziArr->p = (int*)calloc(nzNum, sizeof(int));
	ziArr->i = (int*)calloc(nzNum, sizeof(int));
	ziArr->x = (double*)calloc(nzNum, sizeof(double));
	ziArr->z = (double*)calloc(nzNum, sizeof(double));
	
	int status;

	while (getLine(f, buf))
	{
		int ap, ai;
		double x;
		sscanf_s(buf, "%d %d %lg \n", &ap, &ai, &x);
		ziArr->p[i] = ap;
		ziArr->i[i] = ai;
		ziArr->x[i] = x;
		i++;
	}
}

sparse_zi_struct* ReturnPsi(cholmod_common * c, int fileNum,
	double * ReconstructionGridCoordinates1, double * ReconstructionGridCoordinates2, int lengthReconstrCoord,
	double * SamplingGridCoordinates1, double * SamplingGridCoordinates2, int lengthSampCoord, double rho)
{
	FILE *fPsi;
	errno_t err;
	cholmod_sparse* phiSparse;
	cholmod_sparse* psiSparse;
	cholmod_sparse* indentityMatrM, *indentityMatrN;

	sparse_zi_struct* psiZi = (sparse_zi_struct*)malloc(sizeof(sparse_zi_struct));

	std::string fileName = "psi" + std::to_string(fileNum) + ".mtx";

	err = fopen_s(&fPsi, fileName.c_str(), "r");
	if (err == 0)
	{
		ReadAndAllocateMatrix(fPsi, psiZi);
		fclose(fPsi);
		return psiZi;
	}
	phiSparse = BsplinePHI(ReconstructionGridCoordinates1, ReconstructionGridCoordinates2, lengthReconstrCoord,
		SamplingGridCoordinates1, SamplingGridCoordinates2, lengthSampCoord, c, fileNum);

	indentityMatrM = cholmod_speye(phiSparse->nrow, phiSparse->nrow, phiSparse->xtype, c);
	cholmod_sparse* psi1 = cholmod_horzcat(indentityMatrM, phiSparse, true, c);
	cholmod_dense *s = cholmod_allocate_dense(1, 1, 1, CHOLMOD_REAL, c);
	((double*)s->x)[0] = -rho;
	indentityMatrN = cholmod_speye(phiSparse->ncol, phiSparse->ncol, phiSparse->xtype, c);
	cholmod_scale(s, CHOLMOD_SCALAR, indentityMatrN, c);
	cholmod_sparse* psi2 = cholmod_horzcat(cholmod_transpose(phiSparse, true, c), indentityMatrN, true, c);
	cholmod_sparse* psi = cholmod_vertcat(psi1, psi2, true, c);

	cholmod_triplet* trPsi = cholmod_sparse_to_triplet(psi, c);
	psiZi->p = (int*)calloc(trPsi->nnz, sizeof(int));
	psiZi->i = (int*)calloc(trPsi->nnz, sizeof(int));
	int status;
	psiZi->x = (double*)calloc(trPsi->nnz, sizeof(double));
	psiZi->z = (double*)calloc(trPsi->nnz, sizeof(double));
	double *trArrayZ = (double*)calloc(trPsi->nnz, sizeof(double));
	psiZi->nrow = trPsi->nrow;
	psiZi->ncol = trPsi->ncol;
	psiZi->nzmax = trPsi->nnz;

	status = umfpack_zi_triplet_to_col(trPsi->nrow, trPsi->ncol, trPsi->nnz, (const int*)trPsi->i, (const int*)trPsi->j,
		(const double*)trPsi->x, (const double*)trArrayZ, psiZi->p, psiZi->i, psiZi->x, psiZi->z, nullptr);

	err = fopen_s(&fPsi, fileName.c_str(), "w");
	if (err == 0)
	{
		fprintf(fPsi, "%d %d %d\n", trPsi->nrow, trPsi->ncol, trPsi->nnz);
		for (int i = 0; i < trPsi->nnz; i++)
		{
			fprintf(fPsi, "%d %d %f \n", ((int*)psiZi->p)[i], ((int*)psiZi->i)[i], ((double*)psiZi->x)[i]);
		}
		fclose(fPsi);
	}

	return psiZi;
}


