
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include "ReadSettings.h"

void ReadSettings(int* numIterations,int* sqrtN, int* overGridFactor, float* rho)
{
	std::ifstream input("..\\InputData\\SpursSettings.txt");
	std::string line;

	while (std::getline(input, line)) {
		std::cout << line << '\n';
		std::istringstream ss(line);
		std::string sVal1, sVal2;;
		std::getline(ss, sVal1, '=');
		std::getline(ss, sVal2, '=');

		if (sVal1.find("numIterations") != std::string::npos)
			*numIterations = std::stoi(sVal2);
		else if (sVal1.find("sqrtN") != std::string::npos)
			*sqrtN = std::stoi(sVal2);
		else if (sVal1.find("overGridFactor") != std::string::npos)
			*overGridFactor = std::stoi(sVal2);
		else if (sVal1.find("rho") != std::string::npos)
			*rho = std::stof(sVal2);
	}
}