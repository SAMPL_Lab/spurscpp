#include "ConstructGridArmSpiral.h"
#include <stdio.h>
#include <math.h>

#define PI 3.14159265

void ConstructGridArmSpiral(int sqrtN, int  M, double* &SamplingGridCoordinates1, double* &SamplingGridCoordinates2)
{
	double A = sqrtN / 2;
	double Ns = sqrt(M / PI);
		
	for (int i = 0; i < M; i++)
	{
		double r = sqrt((double)i/M);
		double w = 2 * PI * Ns * r;
		(SamplingGridCoordinates1)[i] = A * r*cos(w);
		(SamplingGridCoordinates2)[i] = A * r*sin(w);
	}
}