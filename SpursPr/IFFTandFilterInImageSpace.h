#pragma once
#include <vector>
#include "CTagCalculation.h"

// Calculate k-space samples on the uniform grid
// Crop to FOV and return real Image
// input parameters:
// double* bReal � real part of the matrix
// double* bImg � imaginary part of the matrix
// int tagSize � matrix size 
// parse_zi_struct* psi � psi sparse matrix
// double* psiZ � imaginary part of the psi
// UmfpackLUParam* luParam � struct with symbolic and numeric part of LU 
// factorization
// int mPhi � number of columns of Phi
// double * imgFilter � image filter
// int sqrtN - points per row / column of the cartesian grid
// int overGridFactor - over sampling factor (default: 2)
// float* outputImage - resulting image
// std::vector<double>& uniformKSamplesRe - k-space samples on the uniform 
// grid, real part
// std::vector<double>& uniformKSamplesIm - k-space samples on the uniform 
// grid, imaginary part
void IFFTandFilterInImageSpace(double* bReal, double* bImg, int tagSize, sparse_zi_struct* psi, double* psiZ, 
	UmfpackLUParam* luParam, int mPhi, double * imgFilter, int sqrtN, int overGridFactor, float* outputImage, std::vector<double>& uniformKSamplesRe, std::vector<double>& uniformKSamplesIm);

