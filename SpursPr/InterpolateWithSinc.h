#pragma once

// Interpolate from Uniform to NonUniform grid
// input parameters:
// int numVar � number of variables
// const double* uniformGridPoints1 � uniform grid
// const double* uniformGridPoints2 � uniform grid
// const double* uniformSampleValuesRe - uniform samples, real part 
// const double* uniformSampleValuesIm � uniform samples, imaginary part
// int numNonUnifGridPoint � number of non-uniform points 
// const double* nonUniformGridPoints1 � non-uniform grid 
// const double* nonUniformGridPoints2 � non-uniform grid
// double* nonUniformSampleValuesOutRe � output values, real part 
// double* nonUniformSampleValuesOutIm � output values, imaginary part
void InterpolateWithSinc(int numVar, const double* uniformGridPoints1, const double* uniformGridPoints2,
	const double* uniformSampleValuesRe, const double* uniformSampleValuesIm,
	int numNonUnifGridPoint, const double* nonUniformGridPoints1, const double* nonUniformGridPoints2,
	double* nonUniformSampleValuesOutRe, double* nonUniformSampleValuesOutIm);