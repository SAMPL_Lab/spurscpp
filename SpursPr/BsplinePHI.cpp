#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>      

#include "BsplinePHI.h"
#include "BsplineFast.h"
#include "../SuiteSparse/UMFPACK/Include/umfpack.h"
#include <string>
#define PI 3.14159265

#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))

cholmod_sparse * BsplinePHI(double * kuReal, double * kuImag, int numKu,
	double * knuReal, double * knuImag, int numKnu, cholmod_common * c, int fileNum)
{
	int spline_degree = 3;
	cholmod_sparse *PHI, *testPhi, *a1;
	errno_t err;
	double *null = (double *)NULL;
	double zero[2] = { 0 };
	double one[2] = { 1, 0 };
	cholmod_triplet *trArray;

	FILE *fPhi;
	std::string fileName = "phi" + std::to_string(fileNum) + ".mtx";
	err = fopen_s(&fPhi, fileName.c_str(), "r");
	if (err == 0)
	{
		printf("The phi file was opened\n");
		PHI = cholmod_read_sparse(fPhi, c);
		fclose(fPhi);
		return PHI;
	}
	else
	{
		printf("The phi file was not opened\n");
	}
	double kuReal2 = (kuReal[0] - kuReal[1])*(kuReal[0] - kuReal[1]); 
	double kuImg2 = (kuImag[0] - kuImag[1])*(kuImag[0] - kuImag[1]);

	double expectedNNZdouble = (1.0 / std::sqrt(kuReal2 + kuImg2)) * spline_degree;
	int expectedNNZ = numKnu * PI * (expectedNNZdouble*expectedNNZdouble) + 0.5;
	int lv = 0;


	trArray = cholmod_allocate_triplet(numKnu, numKu, expectedNNZ, 0, CHOLMOD_REAL, c);

	int max_size = 3000; 
	size_t iimax = (numKnu + 1) / max_size;
	size_t jjmax = (numKu + 1) / max_size;
	size_t rowStemp = 0;
	size_t colStemp = 0;
	for (size_t ii = 0; ii < iimax; ii++)
	{
		size_t iim = ii*max_size;
		size_t iin = MIN((ii + 1)*max_size, numKnu);
		for (size_t jj = 0; jj < jjmax; jj++)
		{
			size_t jjm = jj*max_size;
			size_t jjn = MIN((jj + 1)*max_size, numKu);
			size_t size1 = jjn - jjm;
			size_t size2 = iin - iim;
			size_t sizePxu = size1*size2;
			double* pxu_m_pxnuReal = new double[sizePxu];
			double* pxu_m_pxnuImag = new double[sizePxu];
			size_t ind = 0;
			for (size_t icol = 0; icol < size1; icol = icol++)
			{
				for (size_t irow = 0; irow < size2; irow = irow++)
				{
					pxu_m_pxnuReal[ind] = kuReal[jjm + icol] - knuReal[iim + irow];
					pxu_m_pxnuImag[ind] = kuImag[jjm + icol] - knuImag[iim + irow];
					ind++;
				}
			}
	
			double* bSplineReal = new double[sizePxu]();
			bool isEmpty;
			BSplineFast(pxu_m_pxnuReal, sizePxu, spline_degree, bSplineReal, isEmpty);
			delete[] pxu_m_pxnuReal;
			if (isEmpty == true)
			{
				delete[] bSplineReal;
				delete[] pxu_m_pxnuImag;
				continue;
			}
			double* bSplineImag = new double[sizePxu]();
			BSplineFast(pxu_m_pxnuImag, sizePxu, spline_degree, bSplineImag, isEmpty);
			delete[] pxu_m_pxnuImag;
			if (isEmpty == true)
			{
				delete[] bSplineReal;
				delete[] bSplineImag;
				continue;
			}
			for (size_t ipcol = 0; ipcol < size2; ipcol++) // size nu
			{
				for (size_t iprow = 0; iprow < size1; iprow++)
				{
					double sVar = bSplineReal[ipcol*size1 + iprow] * bSplineImag[ipcol*size1 + iprow];
					if (sVar != 0)
					{
						((int*)trArray->i)[lv] = iprow + iim; //row
						((int*)trArray->j)[lv] = ipcol + jjm; //column
						((double*)trArray->x)[lv] = sVar;
						trArray->nnz++;
						lv++;
					}
				}
			}
			delete[] bSplineReal;
			delete[] bSplineImag;
		}
	}
	
	PHI = cholmod_triplet_to_sparse(trArray, lv, c);
	FILE *fPsi;
	err = fopen_s(&fPsi, fileName.c_str(), "w");
	if (err == 0)
	{
		cholmod_write_sparse(fPsi, PHI, nullptr, nullptr, c);
		fclose(fPsi);
	}

	return PHI;
}

