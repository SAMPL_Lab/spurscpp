#ifndef BSPLINE_FAST_H
#define BSPLINE_FAST_H

// Include Files
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


// Part of the building Bspline
// input parameters: 
// double *tArray � input matrix
// size_t sizeT � size of matrix T
// const int splineDegree � spline degree
// double *xArray � result matrix
// bool &isEmpty � if result matrix is empty
// en.wikipedia.org/wiki/B-spline
void BSplineFast(double *tArray, size_t sizeT, const int splineDegree, double *xArray, bool &isEmpty);
#endif
