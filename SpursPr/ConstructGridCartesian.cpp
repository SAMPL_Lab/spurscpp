#include "ConstructGridArmSpiral.h"
#define PI 3.14159265

void ConstructGridCartesian(int N, int delta, int xOffset, int yOffset, int overGridFactor, double* &grid1, double* &grid2)
{
	double newOverGridFactor = (N*overGridFactor / 2) * 2 / N;
	int numVar = N * newOverGridFactor;

	double*  ukx = new double[numVar];
	double*  uky = new double[numVar];		
	double var = 1 / newOverGridFactor;
	for (int i = 0; i < numVar; i++)
	{
		double tempV = (var - N / 2 - 1 / newOverGridFactor)*delta;
		ukx[i] = tempV + xOffset;
		uky[i] = tempV + yOffset;
		var += (1 / newOverGridFactor);
	}
	for (int rCoord = 0; rCoord < numVar; rCoord++)
	{
		for (int cCoord = 0; cCoord < numVar; cCoord++)
		{
			grid1[rCoord*numVar + cCoord] = ukx[rCoord];
			grid2[rCoord*numVar + cCoord] = uky[cCoord];
		}
	}
	delete[] ukx;
	delete[] uky;
	var += (1 / newOverGridFactor);

	return;
}