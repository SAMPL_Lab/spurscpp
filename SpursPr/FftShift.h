#pragma once
#include <algorithm>
#include <vector>


// For 2D shift need to fftshift first, transpose the matrix, then fftshift it again. Then transpose back to the original matrix form.

template<typename T>
inline void Transpose(std::vector<T>& matrix, int row, int col)
{
	if (row != col) return;
	for (int i = 0; i < row; i++)
	{
		for (int j = i + 1; j < col; j++)
		{
			T temp = matrix[i * col + j];
			matrix[i * col + j] = matrix[j * col + i];
			matrix[j * col + i] = temp;
		}
	}
}

template<typename T>
inline void FftShift2D(std::vector<T> &in, int ydim, int xdim)
{
	std::rotate(in.begin(), in.begin()+(ydim/ 2)*xdim, in.end());
	Transpose<T>(in, ydim, xdim);
	std::rotate(in.begin(), in.begin()+(ydim/ 2)*xdim, in.end());
	Transpose<T>(in, ydim, xdim);

	return;
}