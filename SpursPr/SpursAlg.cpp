#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>   
#include <complex>
#include <algorithm>    // std::transform
#include <ctime>
#include "../SuiteSparse/UMFPACK/Include/umfpack.h"
#include "SpursAlg.h"
#include "ReturnPsi.h"
#include "ConstructGridArmSpiral.h"
#include "ConstructGridCartesian.h"
#include "FactorPsi.h"
#include "IFFTandFilterInImageSpace.h"
#include "ReturnFilter.h"
#include "InterpolateWithSinc.h"
#include <iterator>
#include "mkl.h"
#include "mkl_pblas.h"

#define min(x,y) (((x) < (y)) ? (x) : (y))
#define PI 3.14159265

int HashPhiCalc(double a1, double a2, double a3, double a4, int num1, int num2, int num3, int num4, int num5, double num6)
{
	return ((int)(a1 * 33999 + a3 * 17 + a2 * 99 + num1 * 5 + (num2+a4) * 171 + num3 * 7+ num4 * 111 + num5*11 + num6)); //no meanning to the numbers, just simple hash calculation
}


/* -------------------------------------------------------------------------- */
/* error: print a message and exit */
/* -------------------------------------------------------------------------- */
static void error(char *message)
{
	printf("\n\n====== error: %s =====\n\n", message);
	exit(1);
}

#define MAXLINE 1024
static int getLine(FILE *f, char *buf)
{
	buf[0] = '\0';
	buf[1] = '\0';
	buf[MAXLINE] = '\0';
	return (fgets(buf, MAXLINE, f) != NULL);
}



void SpursAlg(cholmod_dense* bSamp, double* SamplingGridCoordinates1, double* SamplingGridCoordinates2, 
	int overGridFactor, int sqrtN, double rho, int numIterations, cholmod_common& c)
{
	std::clock_t start;
	double duration;

	start = std::clock();
	int numVar = sqrtN * overGridFactor;
	double * reconstructionGridCoordinates1 = new double[numVar*numVar];
	double * reconstructionGridCoordinates2 = new double[numVar*numVar];
	ConstructGridCartesian(sqrtN, 1, 0, 0, overGridFactor, reconstructionGridCoordinates1, reconstructionGridCoordinates2);

	int bsplineDegree = 3;
	double* imageFilter = ReturnFilter(bsplineDegree, overGridFactor, sqrtN);
	int fileNum = HashPhiCalc(reconstructionGridCoordinates2[3], (reconstructionGridCoordinates1[1] - reconstructionGridCoordinates1[0]),
		SamplingGridCoordinates2[3], (SamplingGridCoordinates1[1] - SamplingGridCoordinates1[0]), sqrtN,
		overGridFactor, bSamp->nrow, 1, 1, rho);

	sparse_zi_struct* psi;
	int mPhi, nPhi;

	mPhi = bSamp->nrow;
	nPhi = numVar*numVar;
	psi = ReturnPsi(&c, fileNum, reconstructionGridCoordinates1, reconstructionGridCoordinates2, numVar*numVar,
			SamplingGridCoordinates1, SamplingGridCoordinates2, bSamp->nrow, rho);
	
	UmfpackLUParam* luParam = new UmfpackLUParam();
	int status;
	int psiSize = psi->nzmax;
	double* psiZ = (double*)calloc(psiSize, sizeof(double));
	int n_row = psi->nrow;
	int n_col = psi->ncol;

	//status = FactorPsi(psi, psiZ, &Symbolic, &Numeric, Control, Info);
	status = umfpack_zi_symbolic(n_row, n_col, (const int*)psi->p, (const int*)psi->i, (const double*)psi->x,
		(const double*)psiZ, &(luParam->symbolic), luParam->control, luParam->info);
	if (status < 0)
	{
		umfpack_zi_report_info(luParam->control, luParam->info);
		umfpack_zi_report_status(luParam->control, status);
		printf("umfpack_di_symbolic failed");
	}

	/* ---------------------------------------------------------------------- */
	/* numeric factorization */
	/* ---------------------------------------------------------------------- */
	status = umfpack_zi_numeric((const int*)psi->p, (const int*)psi->i, (const double*)psi->x, (const double*)psiZ,
		luParam->symbolic, &(luParam->numeric), luParam->control, luParam->info);
	if (status < 0)
	{
		umfpack_zi_report_info(luParam->control, luParam->info);
		umfpack_zi_report_status(luParam->control, status);
		printf("umfpack_di_numeric failed");
	}

	/////////////////////////////////
	int bSize = bSamp->nrow;
	int tagSize = bSize + nPhi;

	std::vector<double> bReal(tagSize, 0);
	std::vector<double> bImg(tagSize, 0);
	int coefC = 1;// 1 << 20;
	for (int i = 0; i < bSize; i++)
	{
		bReal[i] = ((double*)bSamp->x)[2 * i]* coefC;
		bImg[i] = ((double*)bSamp->x)[2 * i + 1]* coefC;
	}
	//////////
	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;

	std::cout << " offline calculation  : " << duration << '\n';
	for (int iter = 0; iter < numIterations; iter++)
	{
		int cSize = tagSize - mPhi;
		start = std::clock();

		std::vector<float> outputImage(sqrtN*sqrtN, 0); // size of outputImage is sqrtN*sqrtN
		std::vector<double> uniformKSamplesRe(cSize, 0);
		std::vector<double> uniformKSamplesIm(cSize, 0);
		IFFTandFilterInImageSpace(bReal.data(), bImg.data(), tagSize, psi, psiZ,
			luParam, mPhi, imageFilter, sqrtN, overGridFactor, 
			outputImage.data(), uniformKSamplesRe, uniformKSamplesIm);
		if (iter == numIterations - 1)
		{
			FILE* ftmp1;
			fopen_s(&ftmp1, "..\\OutputData\\OutputImage.mtx", "w");
			for (size_t ind = 0; ind < sqrtN*sqrtN; ind++)
			{
				fprintf_s(ftmp1, "%f ", (double)(outputImage[ind] * 1000));
			}
			fclose(ftmp1);
		}
		std::vector<double> bHatRe(cSize, 0);
		std::vector<double> bHatIm(cSize, 0);
		InterpolateWithSinc(numVar, reconstructionGridCoordinates1, reconstructionGridCoordinates2,
			uniformKSamplesRe.data(), uniformKSamplesIm.data(),
			mPhi, SamplingGridCoordinates1, SamplingGridCoordinates2,
			bHatRe.data(), bHatIm.data());

		std::vector<double> sampleErrRe(tagSize, 0);
		std::vector<double> sampleErrIm(tagSize, 0);
		for (int i = 0; i < bSize; i++)
		{
			sampleErrRe[i] = ((double*)bSamp->x)[2 * i] - (double)bHatRe[i];
			sampleErrIm[i] = ((double*)bSamp->x)[2 * i + 1] - (double)bHatIm[i];
		}

		//calc optimal Alpha
		
		//	float* uniformKSamples = (float*)calloc(cSize, sizeof(float));
		std::vector<double> sample_err_uniform_k_samplesRe(cSize, 0);
		std::vector<double> sample_err_uniform_k_samplesIm(cSize, 0);
		IFFTandFilterInImageSpace(sampleErrRe.data(), sampleErrIm.data(), tagSize, psi, psiZ, 
			luParam, mPhi, imageFilter, sqrtN, overGridFactor,
			outputImage.data(), sample_err_uniform_k_samplesRe, sample_err_uniform_k_samplesIm);

		std::vector<double> sample_err_b_hatRe(cSize, 0);
		std::vector<double> sample_err_b_hatIm(cSize, 0);
		InterpolateWithSinc(numVar, reconstructionGridCoordinates1, reconstructionGridCoordinates2,
			sample_err_uniform_k_samplesRe.data(), sample_err_uniform_k_samplesIm.data(),
			mPhi, SamplingGridCoordinates1, SamplingGridCoordinates2,
			sample_err_b_hatRe.data(), sample_err_b_hatIm.data());


		double sumErrorHat = 0;
		for (int i = 0; i < cSize; i++)
		{
			std::complex<double> tempCompl(sample_err_b_hatRe[i], sample_err_b_hatIm[i]);
			double tempVal = std::abs(tempCompl);
			sumErrorHat += tempVal*tempVal;
		}
		double tempC = 0;
		for (int i = 0; i < bSize; i++)
		{
			std::complex<double> tempSampErr(sampleErrRe[i], sampleErrIm[i]);
			std::complex<double> tempCompl(sample_err_b_hatRe[i], sample_err_b_hatIm[i]);
			tempC += (tempSampErr*tempCompl).real();
		}
		double alpha = tempC / sumErrorHat;
		//	alpha = real(sample_err'*sample_err_b_hat/sum((abs(sample_err_b_hat)).^2));
		for (int i = 0; i < bSize; i++)
		{
			bReal[i] += alpha*sampleErrRe[i];
			bImg[i] += alpha*sampleErrIm[i];
		}
		duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;

		std::cout << "iteration : " << iter << "   " << duration << '\n';
	}
	if(psiZ != NULL) free(psiZ);	
	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << "duration : " << duration << '\n';}
