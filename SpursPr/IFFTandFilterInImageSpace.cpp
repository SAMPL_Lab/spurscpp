#include "IFFTandFilterInImageSpace.h"
#include "ipp.h"
#include <stdio.h>
#include <math.h>  
#include "FftShift.h"
#include "CTagCalculation.h"
#include <vector>

inline void check_sts(int st)
{
	if ((st) != ippStsNoErr)
		printf("Exit status %d \n", (int)st);
}

void Transpose1(std::vector<float> &matrix, int row, int col)
{
	if (row != col) return;
	for (int i = 0; i < row; i++)
	{
		for (int j = i + 1; j < col; j++)
		{
			float temp = matrix[i * col + j];
			matrix[i * col + j] = matrix[j * col + i];
			matrix[j * col + i] = temp;
		}
	}
}

inline void ConvertArrToIppData(std::vector<float>& arrayRe, std::vector<float>& arrayIm, const size_t sizeArr, Ipp32fc* &outArr, int coef = 1)
{
	for (int i = 0; i < sizeArr; i++)
	{
		//	Ipp32fc var = { cArrayRe[i], cArrayIm[i] };
		outArr[i].re = (Ipp32f)arrayRe[i] * coef;
		outArr[i].im = (Ipp32f)arrayIm[i] * coef;
	}
}

void IFFTandFilterInImageSpace(double* bReal, double* bImg, int tagSize, sparse_zi_struct* psi, double* psiZ,
	UmfpackLUParam* luParam, int mPhi, double * imgFilter, int sqrtN, int overGridFactor, float* outputImage, std::vector<double>& uniformKSamplesRe, std::vector<double>& uniformKSamplesIm)
{
	int sizeC = tagSize - mPhi;
	std::vector<float> cArrayRe(sizeC, 0);
	std::vector<float> cArrayIm(sizeC, 0);
	CTagCalculation(bReal, bImg, tagSize, psi, psiZ, luParam, mPhi, cArrayRe, cArrayIm);

	IppStatus           status;
	IppiFFTSpec_C_32fc  *pSpec = nullptr;                              /* Pointer to FFT spec structure */

	int sqrtDim = (int)sqrt(sizeC);
	int log2Res = (int)log2(sqrtDim);
	int colNum, rowNum;

	//start of IFFT_c = ifftshift(ifft2(fftshift(c)));
	FftShift2D<float>(cArrayRe, sqrtDim, sqrtDim);
	FftShift2D<float>(cArrayIm, sqrtDim, sqrtDim);

	Ipp32fc* srcFwd = (Ipp32fc*)ippMalloc(sqrtDim * sqrtDim * sizeof(Ipp32fc) / sizeof(Ipp8u));
	Ipp32fc* IFFT_c = (Ipp32fc*)ippMalloc(sqrtDim * sqrtDim * sizeof(Ipp32fc) / sizeof(Ipp8u));

	Ipp8u  *pMemInit = nullptr, *pBuffer = nullptr;          /* Pointer to the work buffers */
	int  sizeSpec = 0, sizeInit = 0, sizeBuf = 0;    /* Size of FFT spec structure, init and work buffers */

	int coef = 1;
	if (abs(cArrayRe[1] < 100)) coef = 262144;
	ConvertArrToIppData(cArrayRe, cArrayIm, sizeC, srcFwd, coef);

	check_sts(status = ippiFFTGetSize_C_32fc(log2Res, log2Res, IPP_FFT_DIV_INV_BY_N, ippAlgHintAccurate, &sizeSpec, &sizeInit, &sizeBuf));

	/* memory allocation */
	pSpec = (IppiFFTSpec_C_32fc*)ippMalloc(sizeSpec);
	pBuffer = (Ipp8u*)ippMalloc(sizeBuf);
	pMemInit = (Ipp8u*)ippMalloc(sizeInit);
	check_sts(status = ippiFFTInit_C_32fc(log2Res, log2Res, IPP_FFT_DIV_INV_BY_N, ippAlgHintAccurate, pSpec, pMemInit));

	/* backward FFT transform */
	check_sts(status = ippiFFTInv_CToC_32fc_C1R(srcFwd, sqrtDim * sizeof(Ipp32fc), IFFT_c, sqrtDim * sizeof(Ipp32fc), pSpec, pBuffer));

	std::vector<float> outputImageTemp(sizeC, 0);
	for (int i = 0; i < sizeC; i++)
	{
		outputImageTemp[i] = ((float)IFFT_c[i].re);
	}

	FftShift2D<float>(outputImageTemp, sqrtDim, sqrtDim);
	//end of IFFT_c = ifftshift(ifft2(fftshift(c)));

	//OutputImage = (sqrtN*OverGridFactor)^2.*IFFT_c.*abs(ImgFilter);
	std::vector<float> outputImageTempFilt(sizeC, 0);
	for (int i = 0; i < sizeC; i++)
	{
		outputImageTempFilt[i] = sizeC * outputImageTemp[i] * abs(imgFilter[i]) / coef;
	}

	int outpImIndex = 0;
	size_t indStart = (sqrtN*overGridFactor) / 2 - sqrtN / 2;
	size_t indEnd = (sqrtN*overGridFactor) / 2 + sqrtN / 2;

	for (size_t col = indStart; col < indEnd; col++)
	{
		for (size_t row = indStart; row < indEnd; row++)
		{
			int t = col*sqrtDim + row;
			outputImage[outpImIndex++] = (float)outputImageTempFilt[col*sqrtDim + row];
		}
	}
	//end of OutputImage = real(OutputImage(FOV_idx,FOV_idx)); 

	FftShift2D<float>(outputImageTempFilt, sqrtDim, sqrtDim);

	//uniform_k_samples = ifftshift(fft2(fftshift(real(OutputImage))));
	check_sts(status = ippiFFTGetSize_C_32fc(log2Res, log2Res, IPP_FFT_DIV_INV_BY_N, ippAlgHintAccurate, &sizeSpec, &sizeInit, &sizeBuf));

	/* memory allocation */
	IppiFFTSpec_C_32fc* pSpecFl = (IppiFFTSpec_C_32fc*)ippMalloc(sizeSpec);
	Ipp8u* pBufferFl = (Ipp8u*)ippMalloc(sizeBuf);
	Ipp8u* pMemInitFl = (Ipp8u*)ippMalloc(sizeInit);

	check_sts(status = ippiFFTInit_C_32fc(log2Res, log2Res, IPP_FFT_DIV_INV_BY_N, ippAlgHintAccurate, pSpecFl, pMemInitFl));

	std::vector<float> zeroArr(sqrtDim * sqrtDim, 0);
	Ipp32fc* uniformKSamplesArr = (Ipp32fc*)ippMalloc(sqrtDim * sqrtDim * sizeof(Ipp32fc) / sizeof(Ipp8u));
	Ipp32fc* outputImageTempArr = (Ipp32fc*)ippMalloc(sqrtDim * sqrtDim * sizeof(Ipp32fc) / sizeof(Ipp8u));

	ConvertArrToIppData(outputImageTempFilt, zeroArr, sizeC, outputImageTempArr);
	/*  perform forward FFT to put source data to frequency domain */

	check_sts(status = ippiFFTFwd_CToC_32fc_C1R(outputImageTempArr, sqrtDim * sizeof(Ipp32fc),
		uniformKSamplesArr, sqrtDim * sizeof(Ipp32fc), pSpecFl, pBufferFl));

	for (int i = 0; i < sizeC; i++)
	{
		uniformKSamplesRe[i] = (double)uniformKSamplesArr[i].re;
		uniformKSamplesIm[i] = (double)uniformKSamplesArr[i].im;
	}

	FftShift2D<double>(uniformKSamplesRe, sqrtDim, sqrtDim);
	FftShift2D<double>(uniformKSamplesIm, sqrtDim, sqrtDim);

	if (pMemInit != nullptr) ippFree(pMemInit);
	if (pSpec != nullptr) ippFree(pSpec);
	if (pBuffer != nullptr) ippFree(pBuffer);

	if (pMemInitFl != nullptr) ippFree(pMemInitFl);
	if (pSpecFl != nullptr) ippFree(pSpecFl);
	if (pBufferFl != nullptr) ippFree(pBufferFl);

	if (srcFwd != nullptr) ippFree(srcFwd);
	if (IFFT_c != nullptr) ippFree(IFFT_c);
}
