#pragma once
#include "..\SuiteSparse\CHOLMOD\Include\cholmod.h"
#include "..\SuiteSparse\UMFPACK\Include\umfpack.h"

// Numeric factorization of Psi matrix
// input parameters:
// cholmod_sparse *psi � Psi matrix
// double* psiZ � imaginary part of the Psi 
// void** Symbolic - symbolic object see UMPACK user guide
// void** Numeric - numeric object see UMPACK user guide 
// double* control - used for settings in UMPACK
// double* info � used as statistic object in UMPACK
int FactorPsi(cholmod_sparse *psi, double* psiZ, void** Symbolic, void** Numeric, double* control, double* info);
