#pragma once
#include "..\SuiteSparse\CHOLMOD\Include\cholmod.h"


// 1D interpolation using spline function.
// PHI: the linear transform matrix s.t. the (DATA@knu)=PHI*(DATA@ku);
// the process performed only ones for a given sampling pattern or 
// trajectory defined by the set of sampling locations
// input parameters:
// ku: the column vector of Cartesian k-space positions,
// double *kuReal, double* kuImag � Real and imaginary parts  
// accordingly
// int numKu � size of ku vector
// knu: the column vector of Non-Cartesian k-space postion.
// double *knuReal, double* knuImag � Real and imaginary parts  
// accordingly
// int numKnu � size of knu vector
// cholmod_common* c - the Common object contains workspace that is
// used between calls to * CHOLMOD routines. 
// int fileNum � name of the file that contains the PHI.
cholmod_sparse* BsplinePHI(double *kuReal, double* kuImag, int numKu, double *knuReal, 
	double* knuImag, int numKnu, cholmod_common* c, int fileNum);//int n, double* uniformGridPoints, double* uniformSampleValues, double* nonUniformGridPoints)

