#pragma once



typedef struct sparse_zi_struct
{
	size_t nrow;	/* the matrix is nrow-by-ncol */
	size_t ncol;
	size_t nzmax;	/* maximum number of entries in the matrix */

	int *p;		    /* p [0..ncol], the column pointers */
	int *i;		    /* i [0..nzmax-1], the row indices */

	double *x;		/* size nzmax  */
	double *z;		/* size nzmax, if present */

} sparse_zi;