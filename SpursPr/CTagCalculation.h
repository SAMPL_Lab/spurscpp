#pragma once
#include "../SuiteSparse/CHOLMOD/Include/cholmod.h"
#include "../SuiteSparse/UMFPACK/Include/umfpack.h"
#include "DefFile.h"
#include <vector>

class UmfpackLUParam
{
public:
	void * symbolic;
	void * numeric;
	double info[UMFPACK_INFO];
	double control[UMFPACK_CONTROL];
};

// Calculate c_tag = Q * (U \ (L \ (P * (R \ b_tag)))) from b_tag
// input parameters:
// double* bReal - real part of b_tag
// double* bImg � imaginary part of b_tag
// int tagSize � matrix size 
// parse_zi_struct* psi � psi sparse matrix
// double* psiZ � imaginary part of the psi
// UmfpackLUParam* luParam � struct with symbolic and numeric part of LU 
// factorization
// int mPhi � number of columns of Phi
// std::vector<float> &cTagXRes - c_tag real part
// std::vector<float> &cTagZRes � c_tag imaginary part
void CTagCalculation(double* bReal, double* bImg, int tagSize, sparse_zi_struct* psi, double* psiZ,
	UmfpackLUParam* luParam, int mPhi,	std::vector<float> &cTagXRes, std::vector<float> &cTagZRes);