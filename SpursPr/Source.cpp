#include <stdio.h>
#include "SpursAlg.h"
#include "ConstructGridArmSpiral.h"
#include "..\SuiteSparse\CHOLMOD\Include\cholmod.h"
#include "..\SuiteSparse\UMFPACK\Include\umfpack.h"
#include "ReadSettings.h"


int main(int argc, char **argv)
{
	cholmod_common cholmodCommon;
	FILE *fB, *fSamplingGridCoord;
	cholmod_dense *SamplingGridCoordinates1;
	cholmod_dense *SamplingGridCoordinates2;
	cholmod_dense *bSamp;
	int statusInt = 0;
	//Spurs Settings, default values
	int numIterations = 20; 
	int sqrtN = 256; 
	int overGridFactor = 2;
	float rho = 0.001;

	ReadSettings(&numIterations, &sqrtN, &overGridFactor, &rho);
	cholmod_start(&cholmodCommon); /* start CHOLMOD */
	
	//read B and Sampling GridCoordinates
	statusInt = fopen_s(&fB, "..\\InputData\\b.mtx", "r");
	if (statusInt == 0)
	{
		printf("The file b was opened\n");
	}
	else
	{
		printf("The file b was not opened\n");
		return 1;
	}
	bSamp = cholmod_read_dense(fB, &cholmodCommon); /* read in a matrix */
	fclose(fB);
	//read coordinates
	statusInt = fopen_s(&fSamplingGridCoord, "..\\InputData\\SamplingGridCoordinates1.mtx", "r");
	if (statusInt == 0)
	{
		printf("The file SamplingGridCoordinates1 was opened\n");
	}
	else
	{
		printf("The file SamplingGridCoordinates1 was not opened\n");
		return 1;
	}
	SamplingGridCoordinates1 = cholmod_read_dense(fSamplingGridCoord, &cholmodCommon); /* read in a matrix */
	fclose(fSamplingGridCoord);

	statusInt = fopen_s(&fSamplingGridCoord, "..\\InputData\\SamplingGridCoordinates2.mtx", "r");
	if (statusInt == 0)
	{
		printf("The file SamplingGridCoordinates2 was opened\n");
	}
	else
	{
		printf("The files SamplingGridCoordinates2 was not opened\n");
		return 1;
	}
	SamplingGridCoordinates2 = cholmod_read_dense(fSamplingGridCoord, &cholmodCommon); /* read in a matrix */
	fclose(fSamplingGridCoord);
	
	//optional
	//int M = bSamp->nrow;
	//double* SamplingGridCoordinates1 = new double[M];
	//double* SamplingGridCoordinates2 = new double[M];
	//ConstructGridArmSpiral(sqrtN, M, SamplingGridCoordinates1, SamplingGridCoordinates2);
		
	SpursAlg(bSamp, ((double*)SamplingGridCoordinates1->x), ((double*)SamplingGridCoordinates2->x),
		overGridFactor, sqrtN, (double) rho, numIterations, cholmodCommon);
	return 0;
}