#include "FactorPsi.h"
#include "..\SuiteSparse\UMFPACK\Include\umfpack.h"

int FactorPsi(cholmod_sparse * psi, double * psiZ, void ** Symbolic, void ** Numeric, double * control, double * info)
{
	int status;
	status = umfpack_zi_symbolic(psi->nrow, psi->ncol, (const int*)psi->p, (const int*)psi->i, (const double*)psi->x,
		(const double*)psiZ, Symbolic, control, info);
	
	/* ---------------------------------------------------------------------- */
	/* numeric factorization */
	/* ---------------------------------------------------------------------- */
	status = umfpack_zi_numeric((const int*)psi->p, (const int*)psi->i, (const double*)psi->x, (const double*)psiZ,
		*Symbolic, Numeric, control, info);

	return status;
}

