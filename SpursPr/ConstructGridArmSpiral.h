#ifndef CONSTRUCT_ARM_SPIRAL
#define CONSTRUCT_ARM_SPIRAL

// Returns a constant speed spiral trajectory.
// input parameters:
// int sqrtN - the trajectory has a support of a circle with radius= sqrtN /2.
// int  M -  Ns = sqrt(M/pi()) yields a close to uniform density spiral.
// double* &SamplingGridCoordinates1 - output coordinates
// double* &SamplingGridCoordinates2 - output coordinates	
void ConstructGridArmSpiral(int sqrtN, int  M, double* &SamplingGridCoordinates1, double* &SamplingGridCoordinates2);

#endif