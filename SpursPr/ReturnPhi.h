#ifndef RETURN_PHI_H
#define RETURN_PHI_H

#include "cholmod.h"
#include "umfpack.h"


cholmod_sparse* ReturnPsi(cholmod_common* c, int* mPhi, int* nPhi);

// Function Declarations
cholmod_sparse* ReturnPhi(double* inputGridCoord1, double* inputGridCoord2, int numInpCoord,
	double* outputGridCoord1, double* outputGridCoord2, int numOutpCoord, int KernelFunctionDegree, bool forceGenrateNew, cholmod_common& c);

#endif
