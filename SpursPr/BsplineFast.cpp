#include "BsplineFast.h"
#include <cmath>
#include <vector>

int factorial(int n)
{
	int i, x = 1;
	for (i = 1; i <= n; i++)
	{
		x *= i;
	}
	return x;
}

int Nchoosek(int n, int k)
{
	if (k == 0) return 1;
	return ((n * Nchoosek(n - 1, k - 1)) / k);
}


void BSplineFast(double *tArray, size_t sizeT, const int splineDegree, double *xArray, bool &isEmpty)
{
	isEmpty = true;
	std::vector<double> relTIdx1; // array of indexes 
	std::vector<double> relTIdx2;
	int counter = 0;

	int relTIdxLength = 0;

	for (int ind = 0; ind < sizeT; ind++)
	{
	    if (std::abs(tArray[ind]) < splineDegree) 
		{
			relTIdx1.push_back(tArray[ind]);
			relTIdx2.push_back(ind);
			relTIdxLength++;
		}
	}
	if (relTIdxLength == 0) return;

	int NCKvDeg = splineDegree + 2;
	int *NCKv;
	NCKv = new int[NCKvDeg];
	for (int ntilerows = 0; ntilerows < NCKvDeg; ntilerows++)
	{
		NCKv[ntilerows] = Nchoosek(splineDegree + 1.0, ntilerows);
	}
	for (int ntilerows = 1; ntilerows < NCKvDeg; ntilerows = ntilerows + 2) //NCKv.*(-1). ^ (sdegree.')
	{
		NCKv[ntilerows] = NCKv[ntilerows] * (-1);
	}

	double factSpline = 1.0 / factorial(splineDegree);

	for (int ic = 0; ic < relTIdxLength; ic++)
	{
		double sum = 0;
		for (int ir = 0; ir < NCKvDeg; ir++)
		{
			double temp = relTIdx1[ic] - ir + (splineDegree + 1) / 2;;
			double X_K_n_1_2 = (temp > 0) ? (pow(temp, splineDegree)) : 0;
			sum += X_K_n_1_2 * NCKv[ir];
		}
		if (sum != 0)
		{
			xArray[(int)relTIdx2[ic]] = factSpline*sum;
			counter++;
		}
	}
	if(counter > 0)
		isEmpty = false;
	delete[] NCKv;
}
