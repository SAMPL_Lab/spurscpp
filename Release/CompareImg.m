A = importdata('../OutputData/OutputImage.mtx');
OutputImage = reshape(A, 256, 256);
figure('Name','SPURS','NumberTitle','off');
imagesc(real(OutputImage));axis square; colormap('gray'); axis off;
try
    B = importdata('../RefImage/refImage.mtx');
    RefImg = reshape(B, 256, 256);
    AnalyzeResult(OutputImage,RefImg,"Compare");
catch
    disp("file /RefImage/refImage.mtx can't find"); 
end