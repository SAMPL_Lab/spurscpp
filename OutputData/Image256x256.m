 A = importdata('OutputImage.mtx');
 OutputImage = reshape(A, 256, 256);
 figure('Name','SPURS 1st It.','NumberTitle','off');
 imagesc(real(OutputImage));axis square; colormap('gray'); axis off;