# SpursCPP

	How to run SpursCpp on Windows ?
		
		******************************************************************************************************************************
		* Requirements: 
		*     - IPP : https://software.intel.com/en-us/intel-ipp
		* 	  - MKL libraries : https://software.intel.com/en-us/mkl
		*	  - MATLAB 2017a (and above)
		*	  - Visual Studio 2015 x64
		*	  - Clone Git repo (using git command or any client) : git clone https://SAMPL_Lab@bitbucket.org/SAMPL_Lab/spurscpp.git
		******************************************************************************************************************************
		
		1. Clone Git repository to your computer : git clone https://SAMPL_Lab@bitbucket.org/SAMPL_Lab/spurscpp.git
	
		2. Install IPP and MKL libraries (This file is free to use after registration to the Intel site) - see Requirements above.
	
		<!-- - Important - -->
		******************************************************************************************************************************
		* Add to "Path" under "User environment" to load the MKL/IPP and Compiler libraries ( for Windows ):
		* Example as default for 64bit the path is: 
		*	ipp : _X_:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2018.2.185\windows\redist\intel64_win\ipp
		*	mkl : _X_:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2018.2.185\windows\redist\intel64_win\mkl
		*	compiler : _X_:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2018.2.185\windows\redist\intel64_win\compiler
		******************************************************************************************************************************
		
		3. Insert your data input files to:
			InputData\b.mtx (vector b, of k-space samples)
			InputData\SamplingGridCoordinates1.mtx
			InputData\SamplingGridCoordinates2.mtx 
			
		4. User configuration file - InputData\SpursSettings.txt  (Change settings if needed - see below default values):
				numIterations - number of algorithm iteration (default: 20)
				sqrtN - points per row / column of the cartesian grid (default: 256)
				overGridFactor - over sampling factor (default: 2)
				rho - regularization parameter (default: 0.001)
		
		5. To run algorithm:
		
			a. -- Without image displaying --
				Run: Release\SpursPr.exe
				The result data is in OutputData folder.
	
			b. -- With image displaying --
				Run: Release\RunAndDisplay.bat
				To compare image with reference, add refImage into path : RefImage\refImage.mtx (refImage is just column-oriented order matrix without comments and size)

		******************************************************************************************************************************
		  Input file are in "mtx" format column-oriented order.
		  For example in MATLAB: 
				A = [1,2;3,4]
		  The Format should be: %%MatrixMarket - matrix array real general
				2 2
				1
				3
				2
				4
		******************************************************************************************************************************
	  
	--------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
	For Developers only (tested on VS2015 x64):

		1. Install IPP and MKL libraries (This file is free to use after registration to the Intel site)- see Requirements above.
		2. Install SuiteSparse library (Tim Davis) 
		3. Include SpursPr into SuiteSparse solution
			…\SuiteSparse
			…\SpursPr
		4. Insert input data files to:
			…\InputData\b.mtx
			…\InputData\SamplingGridCoordinates1.mtx
			…\InputData\SamplingGridCoordinates2.mtx
		5. Build and run solution.
		6. The result data is in OutputData folder:
			…\OutputData

		
